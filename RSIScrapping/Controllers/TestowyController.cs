﻿using RSICommons.CommonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Google.Apis.Books.v1;
using Google.Apis.Services;
using Newtonsoft.Json;
using RSICommons.CommonModels.DTO;

namespace RSIScrapping.Controllers
{
    public class TestowyController : ApiController
    {
        public HttpResponseMessage GetTest()
        {
            var sth = new List<string> { "test", "test2" };
            return Request.CreateResponse(HttpStatusCode.OK, new ApiResult { status = true, result = sth });
        }

        [HttpGet]
        public HttpResponseMessage GetProductInformation(string langRestrict, int maxResults, string productTitle, string productAuthor = "")
        {
            var service = new BooksService(new BaseClientService.Initializer
            {
                ApplicationName = "GoogleBooksApi",
                ApiKey = "AIzaSyA6eUAQ6GWVjJOqTgMWqWfxi4q_hYKIvb4",
            });
            
            var resultItem = string.IsNullOrEmpty(productAuthor) ? service.Volumes.List("intitle:" + productTitle) : service.Volumes.List("intitle:" + productTitle + "+inauthor:" + productAuthor);
            
            resultItem.LangRestrict = "pl";
            resultItem.MaxResults = maxResults;
            resultItem.OrderBy = VolumesResource.ListRequest.OrderByEnum.Relevance;
           // resultItem.Projection = VolumesResource.ListRequest.ProjectionEnum.Lite;
            var result = resultItem.Execute();

            var products = new List<Product>();
            if (result.Items != null)
            {
                var length = result.Items.Count > maxResults ? maxResults : result.Items.Count;
                for (int i = 0; i < length; i++)
                {
                    Product p = new Product();
                    p.Title = result.Items[i].VolumeInfo.Title == null
                           ? ""
                           : result.Items[i].VolumeInfo.Title;

                    p.Description = result.Items[i].VolumeInfo.Description == null
                        ? ""
                        : result.Items[i].VolumeInfo.Description;

                    if (result.Items[i].VolumeInfo.ImageLinks != null)
                    {
                        p.Thumbnail = result.Items[i].VolumeInfo.ImageLinks.Thumbnail;
                    }
                    p.Genre = "";
                    bool isFirst = true;
                    if (result.Items[i].VolumeInfo.Categories != null)
                    {
                        foreach (string category in result.Items[i].VolumeInfo.Categories)
                        {
                            if (isFirst)
                            {
                                p.Genre += category;
                                isFirst = false;
                            }
                            else
                            {
                                p.Genre += ", " + category;
                            }
                        }
                    }

                    p.Author = "";
                    isFirst = true;
                    if (result.Items[i].VolumeInfo.Authors != null)
                    {
                        foreach (string authories in result.Items[i].VolumeInfo.Authors)
                        {
                            if (isFirst)
                            {
                                p.Author += authories;
                                isFirst = false;
                            }
                            else
                            {
                                p.Author += ", " + authories;
                            }
                        }
                    }

                    p.Publisher = result.Items[i].VolumeInfo.Publisher == null
                        ? ""
                        : result.Items[i].VolumeInfo.Publisher;

                    p.PublishDate = result.Items[i].VolumeInfo.PublishedDate == null
                        ? ""
                        : result.Items[i].VolumeInfo.PublishedDate;

                    p.Language = result.Items[i].VolumeInfo.Language == null
                        ? ""
                        : result.Items[i].VolumeInfo.Language;
                    if (result.Items[i].SaleInfo.ListPrice != null)
                    {
                        string amount = result.Items[i].SaleInfo.ListPrice.Amount.ToString();
                        string currencyCode = result.Items[i].SaleInfo.ListPrice.CurrencyCode.ToString();
                        p.Price = amount + currencyCode;
                    }
                    p.PageCount = result.Items[i].VolumeInfo.PageCount.GetValueOrDefault();
                    products.Add(p);
                }
            }
            if (!products.Any())
                return Request.CreateResponse(HttpStatusCode.NoContent);
            return Request.CreateResponse(HttpStatusCode.OK, products);


        }

        [HttpGet]
        public HttpResponseMessage GetProductInformationByAuthor(String authorName)
        {
            var service = new BooksService(new BaseClientService.Initializer
            {
                ApplicationName = "GoogleBooksApi",
                ApiKey = "AIzaSyA6eUAQ6GWVjJOqTgMWqWfxi4q_hYKIvb4"
            });
            var result = service.Volumes.List(authorName + "&projection=lite").Execute();

           // string title = result.Items.FirstOrDefault().VolumeInfo.Title;
            // if(result.Items.FirstOrDefault().VolumeInfo.Subtitle)
           // string subtitle = result.Items.FirstOrDefault().VolumeInfo.Subtitle;

          //  string ratingCount = result.Items.FirstOrDefault().VolumeInfo.RatingsCount.ToString();
          //  string publisher = result.Items.FirstOrDefault().VolumeInfo.Publisher;
           // string publishedDate = result.Items.FirstOrDefault().VolumeInfo.PublishedDate;
           // string previewLink = result.Items.FirstOrDefault().VolumeInfo.PreviewLink;
           // string maturityRating = result.Items.FirstOrDefault().VolumeInfo.MaturityRating;
          //  string language = result.Items.FirstOrDefault().VolumeInfo.Language;
          //  IList<string> authors = result.Items.FirstOrDefault().VolumeInfo.Authors;

            // List<string> authors = new List<string>();
            //result.Items.FirstOrDefault().VolumeInfo.Authors.ToList<string>;
            return Request.CreateResponse(HttpStatusCode.OK, result.Items);


        }


        //var result = service.Volumes.List("intitle:" + productTitle + "&maxResults=1").ExecuteAsync();

    }
}
