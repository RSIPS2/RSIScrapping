﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
namespace CrudApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "GetProductInformation",
                routeTemplate: "api/GetProductInformation/{langRestrict}/{maxResults}/{productTitle}/{productAuthor}",
                defaults: new { controller = "Testowy", action = "GetProductInformation", productAuthor = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "GetProductInformationByAuthor",
               routeTemplate: "api/Testowy/GetProductInformationByAuthor/{authorName}",
               defaults: new { controller = "Testowy", action = "GetProductInformationByAuthor" }
           );

            config.Routes.MapHttpRoute(
                name: "MapByAction",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}